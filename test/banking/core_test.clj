(ns banking.core-test
  (:require [clojure.test :refer :all]
            [banking.core :refer :all]
            [banking.bank :refer :all]
            [banking.audit_log :refer :all]
            [clojure.data.json :as json]))

(def json-test-data "{\n\"name\": \"Foo\"\n}")

(defn- reset-repositories []
  (reset! bank-account-repository {})
  (reset! audit-log {}))

(deftest get-json-value-returns-the-expected-value
  (testing "testing that get-json-value returns the correct value"
    (is (= "Foo" (get-json-value "name" (json/read-str json-test-data))))))

(deftest new-bank-account-returns-account-with-id-nil
  (testing "testing that the new-bank-account function returns an account with id nil, the id is set when added")
  (is (nil? (:account-number (new-back-account "Foo")))))

(deftest add-account-to-repository-sets-the-id-of-the-bank-account
  (testing "Testing that the account id is set when added to the repo"
    (let [account (new-back-account "Foo")
          acc (add-account-to-repository account)]
      (is (not (nil? (:account-number acc))))
      (reset-repositories))))

(deftest account-exists?-returns-false-when-account-id-is-nil
  (testing "testing that an account without id is always new"
    (is (false? (account-exists? (new-back-account "Foo"))))))

(deftest account-exists?-returns-true-when-account-is-in-repository
  (testing "Testing that the account id is set when added to the repo"
    (let [account (new-back-account "Foo")
          acc (add-account-to-repository account)]
      (is (true? (account-exists? acc)))
      (reset-repositories))))

(deftest add-account-to-repository-increments-the-id-automatically
  (testing "Testing that the account id incremented when added to the repo"
    (let [account (new-back-account "Foo")
          acc (add-account-to-repository account)
          acc2 (add-account-to-repository account)]
      (is (< (:account-number acc) (:account-number acc2)))
      (reset-repositories))))

(deftest add-account-to-repository-returns-same-account-when-it-already-exists
  (testing "Testing that when an existing account is added to the repo, throws an exception (no duplicates!)"
    (let [account (new-back-account "Foo")
          acc (add-account-to-repository account)]
      (is (thrown? Exception (add-account-to-repository acc)))
      (reset-repositories))))

(deftest add-account-to-repository-adds-a-new-account-to-the-repository
  (testing "Testing that the account is added to the repo when add-account-to-repository is called"
    (let [account (new-back-account "Foo")
          acc (add-account-to-repository account)]
      (is (not= (count @bank-account-repository) 0))
      (reset-repositories))))

(deftest remove-account-from-repository-removes-the-given-account-from-the-repository
  (testing "Testing that the account is removed from the repo when remove-account-from-repository is called"
    (let [account (new-back-account "Foo")
          acc (add-account-to-repository account)]
      (is (not= (count @bank-account-repository) 0))
      (remove-account-from-repository acc)
      (is (= (count @bank-account-repository) 0)))))

(deftest show-account-details-returns-error-page-when-account-is-not-found
  (testing "when an account is not found the details page shows an error"
    (is (= 404 (:status (get-account-details 1))))))

(deftest get-account-by-account-number-returns-the-expected-account-from-the-repository
  (testing "Testing that the account is removed from the repo when remove-account-from-repository is called"
    (let [account (new-back-account "Foo")
          acc (add-account-to-repository account)]
      (is (= "Foo" (:name (get-account-by-id (:account-number acc)))))
      (reset-repositories)
      (is (= (count @bank-account-repository) 0)))))

(deftest cannot-deposit-money-when-account-is-not-found
  (testing "You must be unable to deposit money when the account is not found"
    (is (= 404 (:status (deposit-money 1 1))))))

(deftest cannot-deposit-money-is-not-a-number
  (testing "You must be unable to deposit when money is not a number"
    (add-account-to-repository (new-back-account "Foo"))
    (is (= 400 (:status (deposit-money 1 "1"))))
    (reset-repositories)))

(deftest cannot-deposit-a-negative-amount-of-money
  (testing "You must be unable to deposit when money is a negative number"
    (add-account-to-repository (new-back-account "Foo"))
    (is (= 400 (:status (deposit-money 1 -1))))
    (reset-repositories)))

(deftest deposit-money-adds-the-amount-to-the-balance-of-the-account
  (testing "When you deposit money, the amount is added to the balance"
    (let [account (new-back-account "Foo")
          acc (add-account-to-repository account)]
      (deposit-money 1 1)
      (is (== 1 (:balance (get-account-by-id 1))))
      (reset-repositories))))

(deftest cannot-withdraw-money-when-account-is-not-found
  (testing "You must be unable to withdraw money when the account is not found"
    (is (= 404 (:status (withdraw-money 1 1))))))

(deftest cannot-withdraw-money-is-not-a-number
  (testing "You must be unable to withdraw when money is not a number"
    (add-account-to-repository (new-back-account "foo"))
    (is (= 400 (:status (withdraw-money 1 "1"))))
    (reset-repositories)))

(deftest cannot-withdraw-a-negative-amount-of-money
  (testing "You must be unable to withdraw when money is a negative number"
    (add-account-to-repository (new-back-account "foo"))
    (is (= 400 (:status (withdraw-money 1 -1))))
    (reset-repositories)))

(deftest cannot-withdraw-a-more-money-than-there-is-in-the-account
  (testing "You must be unable to withdraw when the amount is greater than balance"
    (add-account-to-repository (new-back-account "foo"))
    (is (= 400 (:status (withdraw-money 1  100))))
    (reset-repositories)))

(deftest withdraw-removes-the-amount-from-the-balance-of-the-account
  (testing "When you deposit money, the amount is added to the balance"
    (let [account (new-back-account "Foo")
          acc (add-account-to-repository account)]
      (deposit-money 1 100)
      (is (== 100 (:balance (get-account-by-id 1))))
      (withdraw-money-from-account 1 95)
      (is (== 5 (:balance (get-account-by-id 1))))
      (reset-repositories))))

(deftest withdraw-money-does-not-accept-a-negative-number
  (testing "When you deposit money, the amount is added to the balance"
    (let [account (new-back-account "Foo")
          acc (add-account-to-repository account)]
      (deposit-money 1 100)
      (is (== 100 (:balance (get-account-by-id 1))))
      (withdraw-money-from-account 1 -1)
      (is (== 100 (:balance (get-account-by-id 1))))
      (reset-repositories))))

(deftest withdraw-money-does-not-accept-an-amount-greater-than-balance
  (testing "When you deposit money, the amount is added to the balance"
    (let [account (new-back-account "Foo")
          acc (add-account-to-repository account)]
      (deposit-money 1 100)
      (is (== 100 (:balance (get-account-by-id 1))))
      (withdraw-money-from-account 1 1000)
      (is (== 100 (:balance (get-account-by-id 1))))
      (reset-repositories))))

(deftest cannot-transfer-money-when-sender-is-not-found
  (testing "You must be unable to transfer money when the sender is not found"
    (is (= 404 (:status (transfer-money 1 2 100))))))

(deftest cannot-transfer-money-receiver-is-not-found
  (testing "You must be unable to transfer money when the receiver is not found"
    (add-account-to-repository (new-back-account "Foo"))
    (is (= 404 (:status (transfer-money 1 2 100))))
    (remove-account-from-repository 1)))

(deftest cannot-transfer-money-when-sender-is-receiver
  (testing "You must be unable to transfer money when sender is equal to the receiver"
    (add-account-to-repository (new-back-account "Foo"))
    (is (= 400 (:status (transfer-money 1 1 100))))
    (reset-repositories)))

(deftest cannot-transfer-when-amount-is-not-a-number
  (testing "You must be unable to transfer when money is not a number"
    (add-account-to-repository (new-back-account "Foo"))
    (add-account-to-repository (new-back-account "Bar"))
    (is (= 400 (:status (transfer-money 1 2 "100"))))
    (reset-repositories)))

(deftest cannot-transfer-when-amount-is-negative
  (testing "You must be unable to transfer when money negative"
    (add-account-to-repository (new-back-account "Foo"))
    (add-account-to-repository (new-back-account "Bar"))
    (is (= 400 (:status (transfer-money 1 2 -1))))
    (reset-repositories)
    (reset-repositories)))

(deftest cannot-transfer-when-amount-is-greater-than-balance
  (testing "You must be unable to transfer when money negative"
    (add-account-to-repository (new-back-account "Foo"))
    (add-account-to-repository (new-back-account "Bar"))
    (is (= 400 (:status (transfer-money 1 2 100))))
    (reset-repositories)))

(deftest transfer-money-withdraws-amount-from-sender-and-deposits-at-receiver
  (testing "Transfer, withdraws from sender and deposits at receiver"
    (add-account-to-repository (new-back-account "Foo"))
    (deposit-money-to-account 1 100)
    (add-account-to-repository (new-back-account "Bar"))
    (transfer-money 1 2 100)
    (is (== 100 (:balance (get-account-by-id 2))))
    (reset-repositories)))

(deftest cannot-view-audit-log-when-account-is-not-found
  (testing "There is no audit-log when the account has not yet been created"
    (is (= 404 (:status (show-audit-log -1))))))

(deftest cannot-view-audit-log-when-account-has-no-transactions
  (testing "There is no audit-log when the account has not yet been used for any transactions"
    (is (= 404 (:status (show-audit-log -1))))))

(deftest audit-log-shows-entry-when-deposit
  (testing "There is an entry in the audit-log when a deposit with the account is made"
    (add-account-to-repository (new-back-account "Foo"))
    (deposit-money-to-account 1 100)
    (is (some? (get-log-by-account-number 1)))
    (reset-repositories)))

(deftest audit-log-adds-entry-when-widthdraw
  (testing "There is an entry in the audit-log when a withdraw with the account is made"
    (add-account-to-repository (new-back-account "Foo"))
    (deposit-money-to-account 1 100)
    (withdraw-money-from-account 1 100)
    (is (== 2 (count (get-log-by-account-number 1))))
    (reset-repositories)))

(deftest audit-log-adds-entry-when-money-transfer
  (testing "There is an entry in the audit-log of sender and receiver when a transfer with the account is made to another"
    (add-account-to-repository (new-back-account "Foo"))
    (add-account-to-repository (new-back-account "Bar"))
    (deposit-money-to-account 1 100)
    (transfer-money-between-accounts 1 2 100)
    (is (== 2 (count (get-log-by-account-number 1))))
    (is (== 1 (count (get-log-by-account-number 2))))
    (reset-repositories)))

(deftest audit-log-ordering-always-shows-last-log-at-index-0
  (testing "There is an entry in the audit-log of sender and receiver when a transfer with the account is made to another"
    (add-account-to-repository (new-back-account "Foo"))
    (deposit-money-to-account 1 100)
    (withdraw-money-from-account 1 50)
    (withdraw-money-from-account 1 45)
    (is (== 2 (get (first (get-log-by-account-number 1)) "sequence")))
    (reset-repositories)))

(deftest audit-log-shows-correct-entry-when-deposit
  (testing "There is a credit entry in the audit-log when a deposit with the account is made"
    (add-account-to-repository (new-back-account "Foo"))
    (deposit-money-to-account 1 100)
    (is (== 100 (get (first (get-log-by-account-number 1)) "credit")))
    (reset-repositories)))

(deftest audit-log-shows-correct-entry-when-withdrawn
  (testing "There is a debit entry in the audit-log when a withdraw with the account is made"
    (add-account-to-repository (new-back-account "Foo"))
    (deposit-money-to-account 1 100)
    (withdraw-money-from-account 1 100)
    (is (== 100 (get (first (get-log-by-account-number 1)) "debit")))
    (reset-repositories)))

(deftest audit-log-of-sender-shows-correct-entry-when-transfer
  (testing "There is a debit entry in the audit-log when a transfer from the account is made"
    (add-account-to-repository (new-back-account "Foo"))
    (add-account-to-repository (new-back-account "Bar"))
    (deposit-money-to-account 1 100)
    (transfer-money-between-accounts 1 2 100)
    (is (== 100 (get (first (get-log-by-account-number 1)) "debit")))
    (is (= "send to #2" (get (first (get-log-by-account-number 1)) "description")))
    (reset-repositories)))

(deftest audit-log-of-receiver-shows-correct-entry-when-transfer
  (testing "There is a credit entry in the audit-log when a transfer to the account is made"
    (add-account-to-repository (new-back-account "Foo"))
    (add-account-to-repository (new-back-account "Bar"))
    (deposit-money-to-account 1 100)
    (transfer-money-between-accounts 1 2 100)
    (is (== 100 (get (first (get-log-by-account-number 2)) "credit")))
    (is (= "receive from #1" (get (first (get-log-by-account-number 2)) "description")))
    (reset-repositories)))

;(deftest multithreading-test
;  (testing "testing if deposit = 10 after deposit 1 on 10 threads"
;    (add-account-to-repository (new-back-account "Foo"))
;    (dotimes [i 10] (.start (Thread. (fn [] (deposit-money-to-account 1 1)))))
;    (Thread/sleep 4000)
;    (is (== 10 (:balance (get-account-by-id 1))))
;    (reset-repositories)))