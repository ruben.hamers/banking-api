# banking

Simple banking api in Clojure.

## Usage
 
Make sure you have leiningen installed. (https://leiningen.org/)

Then, Run the server with `lein run` and navigate to `http://127.0.0.1:3000/`


## Options

The api supports the following routes:

|Http method   | Route  | Request body example  | Description
|---|---|---|---|
|POST   |*/account   |{"name": "Mr. Black"}   |Adds a new account   |
|GET   |*/account/:id   |   |Gets an account by id   |
|POST   |*/account/:id/deposit   |{"amount": 100}   |Deposits the amount to the balance of the account owner   |
|POST|*/account/:id/withdraw|{"amount": 5}|Withdraws the amount from the balance of the account owner|
|POST|*/account/:id/send|{"amount": 50, "account-number": 800}|Transfers money from one account to another|
|GET|*/account/:id/audit||Gets the audit log of an account| 

### Bugs

- Threading does not yet fully work

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
