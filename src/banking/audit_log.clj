(ns banking.audit_log)

;; audit log
;; {account-number [{log-entry} {log-entry}]}
(defonce audit-log (atom {}))

(defn get-log-by-account-number
  "Get the audit-log of an account by its account number"
  [account-number]
  (get @audit-log account-number))

(defn- add-log-entry
  "add a log-entry to the log"
  [account-number action description]
  (let [existing-log (if (nil? (get @audit-log account-number)) () (get @audit-log account-number))
        entry (conj {"sequence" (count existing-log)} action description)]
    (swap! audit-log assoc account-number (conj existing-log entry))))

(defn log-deposit
  "deposit log helper function"
  [account-number amount]
  (add-log-entry account-number {"credit" amount} {"description" "deposit"}))

(defn log-withdraw
  "withdraw log helper function"
  [account-number amount]
  (add-log-entry account-number {"debit" amount} {"description" "withdraw"}))

(defn log-transfer
  "transfer log helper function"
  [sender receiver amount]
  (add-log-entry sender {"debit" amount} {"description" (format "send to #%s" receiver)})
  (add-log-entry receiver {"credit" amount} {"description" (format "receive from #%s" sender)}))