(ns banking.bank
  (:require [banking.audit_log :as log]))

;; record to define what a bank account is
(defrecord bank-account [account-number name balance])
;; this should be something persistent in the future
;; {:account-number bank-account}
(defonce bank-account-repository (atom {}))

(defn get-account-by-id
  "get an account by account-number"
  [id]
  (get @bank-account-repository id))

(defn account-exists?
  "Check if an account exists"
  [account]
  (cond
    (number? account)
    (some? (get-account-by-id account))
    (nil? (:account-number account))
    false
    :else
    (some? (get-account-by-id (:account-number account)))))

(defn add-account-to-repository
  "Add an account to the bank-repository"
  [account]
  (if (account-exists? account)
    (throw (Exception. "The account you are trying to add already exists!"))
    (let [number (+ 1 (count @bank-account-repository))
          updated-account (assoc account :account-number number)]
      (swap! bank-account-repository assoc number updated-account)
      updated-account)))

(defn remove-account-from-repository
  "Remove something from the repository"
  [account]
  (if (number? account)
    (let [account (get-account-by-id account)]
      (swap! bank-account-repository dissoc (:account-number account))
      account)
    (do (swap! bank-account-repository dissoc (:account-number account))
        account)))

(defn new-back-account
  "Generate a new bank account"
  [name]
  (->bank-account nil name 0))

(defn- deposit
  "Make a deposit into an account when the deposit is greater-or-equal than 0.
  Always returns the account"
  [account-number amount]
  (when (>= amount 0)
    (swap! bank-account-repository assoc account-number
           (let [account (get-account-by-id account-number)]
             (when (some? account)
               (assoc account :balance (+ (:balance account) amount))))))
  (get-account-by-id account-number))

(defn deposit-money-to-account
  "Make a deposit into an account when the deposit is greater-or-equal than 0.
  Always returns the account"
  [account-number amount]
  (let [account (deposit account-number amount)]
    (log/log-deposit (:account-number account) amount)
    account))

(defn can-withdraw?
  "check if the amount is able to be withdrawn from the account"
  [account amount]
  (and (>= amount 0)
       (<= amount (:balance
                    (if (number? account)
                      (get-account-by-id account)
                      account)))))

(defn- withdraw
  "Withdraw money from an account when the amount is greater-or-equal than 0.
  Always returns the account"
  [account-number amount]
  (swap! bank-account-repository assoc account-number
         (let [account (get-account-by-id account-number)]
           (if (can-withdraw? account amount)
             (assoc account :balance (- (:balance account) amount))
             account)))
  (get-account-by-id account-number))

(defn withdraw-money-from-account
  "Withdraw money from an account when the amount is greater-or-equal than 0.
  Always returns the account"
  [account-number amount]
  (let [account (withdraw account-number amount)]
    (log/log-withdraw (:account-number account) amount)
    account))

(defn transfer-money-between-accounts
  "transfers money between accounts.
  First withdraws from sender, then deposits to receiver"
  [sender-id receiver-id amount]
  (withdraw sender-id amount)
  (deposit receiver-id amount)
  (log/log-transfer sender-id receiver-id amount)
  (get-account-by-id (:account-number sender-id)))
