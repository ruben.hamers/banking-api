(ns banking.core
  (:require [org.httpkit.server :as server]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer :all]
            [clojure.data :refer :all]
            [clojure.string :as str]
            [clojure.data.json :as json]
            [banking.bank :as bank]
            [banking.audit_log :as log])
  (:gen-class))

(defn get-json-value
  "Get a value from a json structure by key"
  [key body]
  (get body key))

(defn encode [body]
  "encode clojure data to json
  There probably is a nice efficient way to do this with clojure ring/compojure but this will do for now"
  (json/write-str body))

(defn get-parameter
  "Get param from request"
  [req name] (get (:params req) name))

(defn decode
  "Decode the request body to json (uses slurp to read body)
  There probably is a nice efficient way to do this with clojure ring/compojure but this will do for now"
  [req]
  (json/read-str (slurp (:body req))))

(defn show-account-not-found-page [id]
  {:status  404
   :headers {"Content-Type" "text/text"}
   :body    (format "Oops, no account found with id: %s" id)})

(defn show-error-page [msg]
  {:status  400
   :headers {"Content-Type" "text/text"}
   :body    (str msg)})

(defn get-account-details [id]
  (let [account (bank/get-account-by-id id)]
    (if (nil? account)
      (show-account-not-found-page id)
      {:status  200
       :headers {"Content-Type" "text/json"}
       :body    (str (encode account))})))

(defn deposit-money
  "Deposit money to an account"
  [account-number deposit]
  (if (not (bank/account-exists? account-number))
    (show-account-not-found-page account-number)
    (if (number? deposit)
      (if (>= deposit 0)
        {:status  200
         :headers {"Content-Type" "text/json"}
         :body    (->> (bank/deposit-money-to-account account-number (cast Long deposit))
                       (encode)
                       (str))}
        (show-error-page "Oops, cannot deposit a negative amount of money!"))
      (show-error-page "Oops, your given deposit is not a number!"))))

(defn withdraw-money
  "withdraw money from an account"
  [account-number amount]
  (cond
    (not (bank/account-exists? account-number))
    (show-account-not-found-page account-number)
    (false? (number? amount))
    (show-error-page "Oops, your given withdraw is not a number!")
    (< amount 0)
    (show-error-page "Oops, cannot withdraw a negative amount of money!")
    (false? (bank/can-withdraw? account-number amount))
    (show-error-page "Oops, cannot withdraw more than is in your account!")
    :else
    {:status  200
     :headers {"Content-Type" "text/json"}
     :body    (->> (bank/withdraw-money-from-account account-number (cast Long amount))
                   (encode)
                   (str))}))

(defn transfer-money
  "transfer money from sender to receiver"
  [sender-id receiver-id amount]
  (let [sender (bank/get-account-by-id sender-id)
        receiver (bank/get-account-by-id receiver-id)]
    (cond
      (== sender-id receiver-id)
      (show-error-page "Oops, cannot send money to your self!")
      (nil? sender)
      (show-account-not-found-page sender-id)
      (nil? receiver)
      (show-account-not-found-page receiver-id)
      (false? (number? amount))
      (show-error-page "Oops, your given transfer is not a number!")
      (< amount 0)                                          ;; not in requirements, but still..
      (show-error-page "Oops, you cannot transfer a negative amount of money!")
      (false? (bank/can-withdraw? sender amount))
      (show-error-page "Oops, cannot transfer more than is in your account!")
      :else
      {:status  200
       :headers {"Content-Type" "text/json"}
       :body    (->> (bank/transfer-money-between-accounts sender-id receiver-id (cast Long amount))
                     (encode)
                     (str))})))

(defn show-audit-log [id]
  (let [log (log/get-log-by-account-number id)]
    (if (nil? log)
      (show-account-not-found-page id)
      {:status  200
       :headers {"Content-Type" "text/json"}
       :body    (str (encode log))})))

(defn add-account-handler [req]
  {:status  200
   :headers {"Content-Type" "text/json"}
   :body    (->> (decode req)
                 (get-json-value "name")
                 (bank/new-back-account)
                 (bank/add-account-to-repository)
                 (encode)
                 (str))})

(defn get-account-details-handler
  "Get account details handler"
  [id]
  (get-account-details (read-string id)))

(defn deposit-money-handler
  "Handler for deposit-money"
  [req]
  (let [account-number (read-string (get-parameter req :id))
        deposit (->> (decode req)
                     (get-json-value "amount"))]
    (deposit-money account-number deposit)))

(defn withdraw-money-handler
  "Handler for withdraw-money"
  [req]
  (let [account-number (read-string (get-parameter req :id))
        deposit (->> (decode req)
                     (get-json-value "amount"))]
    (withdraw-money account-number deposit)))

(defn transfer-money-handler
  "Handler for withdraw-money"
  [req]
  (let [sender-id (read-string (get-parameter req :id))
        json (decode req)
        receiver-id (get-json-value "account-number" json)
        amount (get-json-value "amount" json)]
    (transfer-money sender-id receiver-id amount)))

(defn show-audit-log-handler
  "show the audit logs from an account"
  [id]
  (show-audit-log (read-string id)))

(defroutes app-routes
           (POST "/account" [] add-account-handler)
           (GET "/account/:id" [id] (get-account-details-handler id))
           (POST "/account/:id/deposit" [id] deposit-money-handler)
           (POST "/account/:id/withdraw" [id] withdraw-money-handler)
           (POST "/account/:id/send" [id] transfer-money-handler)
           (GET "/account/:id/audit" [id] (show-audit-log-handler id))
           (route/not-found "Error, page not found!"))

(defn -main
  "Main"
  [& args]
  (let [port (Integer/parseInt (or (System/getenv "PORT") "3000"))]
    (server/run-server (wrap-defaults #'app-routes api-defaults) {:port port})
    (println (str "Running banking-api at http:/127.0.0.1:" port "/"))))